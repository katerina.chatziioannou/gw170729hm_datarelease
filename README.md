# On the properties of the heavy binary black hole merger GW170729

Data release for https://arxiv.org/abs/1903.06742. If you use data from this repository, please cite the paper.

## Compact binary samples

We release samples from the posterior distributions of the parameters of the compact binary for the analyses presented in Tables II and III 
(apart from `IMRPhenomPv2 (χ)` and `SEOBNRv3 (χ)` whose samples were released by the LIGO and Virgo Collaborations, and can be found at https://dcc.ligo.org/LIGO-P1800370/public).

For example `TableII_uniform-in-chi.hdf5` contains samples from the runs of Table II that used the uniform-in-chi spin prior (following notation from Table II): `IMRPhenomD (χ)`, `IMRPhenomHM (χ)`, `SEOBNRv4 (χ)`, `SEOBNRv4HM (χ)`

To see the waveform models available in each file,

```
import h5py
BBH_data = h5py.File('TableII_uniform-in-chi.hdf5', 'r')
BBH_data.keys()
```
This should give 
```
[u'IMRPhenomD', u'IMRPhenomHM', u'SEOBNRv4', u'SEOBNRv4HM']
```
For all waveforms we release samples for the following parameters

1. `costheta_jn`: The cosine (rad) of the angle between the total angular momentum of the binary and the line of sight.
2. `luminosity_distance_Mpc`: The luminosity distance to the source in units of Mpc.
3. `right_ascension` and `declination`: The right ascension and declination of the sky position of the binary (rad).
4. `m1_detector_frame_Msun`: The detector-frame mass of the primary in units of solar masses.
5. `m2_detector_frame_Msun`: The detector-frame mass of the secondary in units of solar masses.
6. `mass_ratio`: The ratio of the masses of the binary components

For spin-precessing waveforms we also release samples for:

7. `spin1`: The magnitude of the dimensionless spin of the primary.
8. `spin2`: The magnitude of the dimensionless spin of the secondary.
9. `costilt1`: The cosine (rad) of the angle between the spin of the primary and the total angular momentum, defined at a GW frequency of 20Hz.
10. `costilt2`: The cosine (rad) of the angle between the spin of the secondary and the total angular momentum, defined at a GW frequency of 20Hz.
11. `chi_eff`: The effective spin of the binary

Finally, for spin-aligned wavefors we also release samples for:

12. `spin1z`: The dimensionless spin of the primary along the orbital angular momentum.
13. `spin2z`: The dimensionless spin of the secondary along the orbital angular momentum.
14. `chi_eff`: The effective spin of the binary
   

## BayesWave Data

The BayesWave data are used to make Figures 7 and 8. We release the credible intervals for the whitened time-domain signal reconstruction for each detector when using wavelets and chirplets. 

For example, `BayesWave/wavelet/signal_median_time_domain_waveform_L1.dat` has the reconstruction of the signal observed by LIGO-Livingston when using wavelets as the basis function. 

Each file has 6 columns, corresponding to

1. time in seconds from the start of the analysis segment
2. median signal reconstruction
3. 25th percentile of the signal reconstruction
4. 75th percentile of the signal reconstruction
5. 5th percentile of the signal reconstruction
6. 95th percentile of the signal reconstruction